# reservoirs-ids.json
This json assings a id to the reservoirs pages, based on it's url number.

Command used to obtain the IDs:
```
ls -1 pantano*  | sed -E 's/^([^0-9]+)([0-9]+)(.+)(.html)/\2: "\/\1\2\3\4",/' | sort -n  | sed -n -E 's/^([0-9]+)/"\1"/p' | head
```
>Some ids like "1" or "5" are not used in the source, for this reason the id numbers are not consecutive.

# json:
```json
{
    "2": "/pantano-2-agueda.html",
    "3": "/pantano-3-aguilar-de-campoo.html",
    "4": "/pantano-4-acebo.html",
    "6": "/pantano-6-la-acena.html",
    "8": "/pantano-8-el-agrio.html",
    "9": "/pantano-9-aguascebas.html",
    "10": "/pantano-10-alcala-del-rio.html",
    "11": "/pantano-11-los-algarbes.html",
    "12": "/pantano-12-el-alisillo.html",
    "13": "/pantano-13-los-almeriques.html",
    ...
}
```

