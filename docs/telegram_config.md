# Set Telegram configuration

Send Telegram messages is optional but if you want to use this feature you must set your own config.

## Copy the telegram config template
It's important copy the Telegram config in the same dir as the file "telegram_bot.cfg.template".

```
cd /to/your/root/repo/clone/dir
```

```
cp -vp lib/telegram/telegram_bot.cfg.template lib/telegram/telegram_bot.cfg
```

Now the symbolic link in the root proyect dir works opening with your favorite file editor, in this case i use vim:

```
vim LINK-TO-telegram_bot.cfg
```

### Set the vars content
All required vars are uncomment in the telegram_bot.cfg with example values, you must redifine this values.

#### BOT_TOKEN
Get your [Telegram bot token](https://core.telegram.org/bots/api#authorizing-your-bot) string

Define in BOT_TOKEN var (if you like for add the comment with your bot name):

```
BOT_TOKEN='<your_bot_token>' # mybot-bot
```

#### CHAT_ID
You can get chat id with multiple methods search in the web "telegram how to get chat id" or follow the "get chat id from curl call" instructions.

If you have the chat_id set it as value:

```
CHAT_ID="-1234567890"
```

If not, you can get the chat id with curl:

##### Get chat id from curl call

To get the chat or group id "[chat_id](https://core.telegram.org/bots/api#getchat)".

- Add the bot-user to the desired Telegram group.
- Grant read permissions to the bot (promote the bot to admin of desired group, this grant read messages access).
+info: [Telegram: what-messages-will-my-bot-get](https://core.telegram.org/bots/faq#what-messages-will-my-bot-get)

When your bot haves message read permissions, with curl and jq you can make a API call to get the Updates like:

```
curl -s https://api.telegram.org/bot<your_bot_token>/getUpdates | jq '.result[].message.chat'
```

If curl call it's ok, jq gets the chat definition inside the messages who are inside the array elements in result like:

```
$ curl -s https://api.telegram.org/bot<your_bot_token>/getUpdates | jq '.result[].message.chat'
{
  "id": -1234567890,
  "title": "<group name>",
  "type": "<type of chat>"
}
{
  "id": ...,
  "title": ...,
  "type": ...
}
null
```

If you don't see any json definition, try to write a message in the group and repeat the api call. Or execute only the curl and analize the output.

+info: [Telegram: get updates](https://core.telegram.org/bots/api#getting-updates)
