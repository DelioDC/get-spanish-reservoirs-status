#!/bin/bash
################################################################################
# get_embalse_status.sh
################################################################################
# Description: Get the spanish reservoirs fillment status.
# Author: DelioDC
# Created at: 2023/08/19
VERSION="0.1"
#################################################################################
# set info (more details in official doc):
# -e  Exit immediately when detects some erros.
# Recomended in develop time:
# -u  Treat unset variables and parameters as error.
# -v  Print shell input lines as they are read.
# +info: "man set" or https://www.gnu.org/software/bash/manual/html_node/The-Set-Builtin.html
set -e #vu
SCRIPT_BASE_PATH=$(cd $(dirname "${BASH_SOURCE[0]}") && pwd)
CONFIG_CUSTOM_PATH="$SCRIPT_BASE_PATH/config.cfg"
TELEGRAM_CONFIG_FILE="./lib/telegram/telegram_bot.cfg"
#################################################################################
# Libraries
#################################################################################
LIB_PATH="$SCRIPT_BASE_PATH/lib"
# Load bash libraries:
. "$LIB_PATH/_libs_config.sh"
for library in "${LIBRARY_FILES[@]}"
do
  if [[ -r "$library" ]]; then
      . $library
  else
      >&2 echo -e  $E_ERROR"ERROR: lib [$library] can't be readed."$E_NC
      exit 1
  fi
done

#################################################################################
# Global variables
#################################################################################
LOG_FILE_NAME="$(basename $0 | sed -E 's/\.sh$//')_$(date +%Y%m%d).log"
#
# Load config and user custom config from config.cfg
#
if [[ -r "$CONFIG_CUSTOM_PATH" ]];
then
    . "$CONFIG_CUSTOM_PATH"
    if [[ ! -z "$LOG_PATH_CUSTOM" ]];
    then
        LOG_PATH="$LOG_PATH_CUSTOM"
        log_write_debug "Loaded LOG_PATH from config.cfg [$LOG_PATH_CUSTOM]"
    else
        LOG_PATH="$SCRIPT_BASE_PATH/$LOG_FILE_NAME"
    fi
    log_write_debug "Loaded custom config from CONFIG_CUSTOM_PATH[$CONFIG_CUSTOM_PATH]"
fi

if [[ ! -z "$LOG_PATH" ]];
then
    log_write_debug "Loaded LOG_PATH from config.cfg [$LOG_PATH]"
else
    LOG_PATH="$SCRIPT_BASE_PATH/$LOG_FILE_NAME"
    log_write_debug "Setted LOG_PATH to default config LOG_PATH[$LOG_PATH]"
fi
log_write_debug "Start execution [$0] with loaded libraries (End line is writed when ends successfully)"


for library in "${OPTIONAL_LIBRARY_FILES[@]}"
do
  if [[ -r "$library" ]]; then
      . $library
  else
      log_write_warn "Optional library [$library] can't be loaded"
  fi
done

FQDN="www.embalses.net"
log_write_debug "Defined FQDN[$FQDN]"

URL_BASE="https://$FQDN"
log_write_debug "Defined URL_BASE[$URL_BASE]"

JSON_RESERVOIRS_IDS_PATH="$LIB_PATH/jsons/reservoirs-ids.json"
log_write_debug "Defined JSON_RESERVOIRS_IDS_PATH[$JSON_RESERVOIRS_IDS_PATH]"

if [[ ! -z "$CACHE_DIR" ]];
then
    log_write_debug "Loaded CACHE_DIR from config.cfg [$CACHE_DIR]"
else
    CACHE_DIR="$HOME/.cache/$(basename $0)"
    log_write_debug "Setted CACHE_DIR to default config CACHE_DIR[$CACHE_DIR]"
fi

if [[ ! -z "$USER_AGENT" ]];
then
    log_write_debug "Loaded USER_AGENT from config.cfg [$USER_AGENT]"
else
    # User agent updated at 20230822: https://www.whatismybrowser.com/guides/the-latest-user-agent/windows
    USER_AGENT='Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:115.0) Gecko/20100101 Firefox/116.0'
    log_write_debug "Setted USER_AGENT to default config USER_AGENT[$USER_AGENT]"
fi

# This global vars must be setted in runtime:
HTML_FULL_RAW=""
JSON_RESERVOIR=""
USER_RESERVOIR_ID=""
RESERVOIR_ID=""
RESERVOIR_URL_RELATIVE=""
MESSAGE_IN_HTML=""

#################################################################################
# Configure environment
#################################################################################
# Create script cache dir if not exists:
if [[ -w "$HOME/.cache" ]];
then
    if [[ ! -d "$CACHE_DIR" ]];
    then
        mkdir -p "$CACHE_DIR"
        log_write_info "Created cache dir [$CACHE_DIR]"
    fi
else
    exit_with_error_message "The user cache dir '$HOME/.cache' don't exists or is unwritable"
fi

URL_DATE="$(date '+%Y%m%d')"
CACHE_FILE_BASE="$CACHE_DIR/$URL_DATE"
log_write_debug "Setted CACHE_FILE_BASE[$CACHE_FILE_BASE]"

# Defined in runtime:
CACHE_FILE=""
#################################################################################
# Get data from internet
#################################################################################

function download_html {
    if [[ ! -z "$1" ]] && [[ ! -z "$RESERVOIR_ID" ]];
    then
        local url="$1"
        local cache_dir="$CACHE_DIR"

        if [[ -d "$cache_dir" ]];
        then
            if [[ ! -z "$CACHE_FILE" ]] && [[ -e "$CACHE_FILE" ]];
            then
                local cache_file_size_bits=$(stat --format=%s "$CACHE_FILE")
                if (( cache_file_size_bits < 10240 ));
                then
                    # If cache file exists, but haves less size than 10KiB:
                    rm "$CACHE_FILE"
                    log_write_debug "removed cache file with size less than 10KiB [$CACHE_FILE]"
                fi
            fi

            if [[ ! -e "$CACHE_FILE" ]];
            then
                if [[ -w "$cache_dir" ]];
                then
                    # Clean previus (all) cache files:
                    rm -fR "$cache_dir/*"
                    log_write_debug "removed all previus cache files in CACHE_DIR [$cache_dir]"

                    # Create the new cache file for today:
                    log_write_debug "Executing [curl -s --user-agent \"$USER_AGENT\" \"$1\"]"
                    curl_call "$url" > "$CACHE_FILE"
                    log_write_ok "Getted new data from url[$url] saved in cache file[$CACHE_FILE]"
                    log_write_info "created new cache file [$CACHE_FILE]"

                else
                    exit_with_error_message "download_html \$cache_dir dir '$cache_dir' not writable."
                fi
            else
                log_write_info "Finded cache file [$CACHE_FILE]"
            fi
        else
            exit_with_error_message "download_html \$cache_dir dir '$cache_dir' not found."
        fi
    else
        exit_with_error_message "download_html not defined url in first parameter [$1] or \$RESERVOIR_ID[$RESERVOIR_ID] not defined."
    fi
}

#################################################################################
# Load external data
#################################################################################
function load_raw_html {
    CACHE_FILE="$CACHE_FILE_BASE""_""$RESERVOIR_ID"
    log_write_debug "Setted CACHE_FILE[${CACHE_FILE}]"

    set_reservoir_relative_url_form_id
    RESERVOIR_URL_FULL="$URL_BASE$RESERVOIR_URL_RELATIVE"
    log_write_debug "Setted RESERVOIR_URL_FULL[${RESERVOIR_URL_FULL}]"

    if [[ ! -z "$RESERVOIR_ID" ]];
    then
        if [[ ! -r "$CACHE_FILE" ]] && [[ ! -z "$RESERVOIR_URL_RELATIVE" ]] ;
        then
            log_write_debug "Calling function [download_html] with parameter RESERVOIR_URL_FULL[${RESERVOIR_URL_FULL}]"
            download_html "$RESERVOIR_URL_FULL"
            log_write_ok "Downloaded raw data from RESERVOIR_URL_FULL[$RESERVOIR_URL_FULL]"
        fi
        if [[ -r "$CACHE_FILE" ]] ;
        then
            log_write_debug "Obtained raw html successfully from RESERVOIR_URL_FULL[$RESERVOIR_URL_FULL] CACHE_FILE[$CACHE_FILE]"
            # remove windows breaklines with "\r", tabs or more than 1 spaces:
            HTML_FULL_RAW=$(sed -E 's/\r//g ; s/\t//g ; s/\s\s+//g' "$CACHE_FILE" | grep -vE "^\s*$" )
            log_write_debug "Setted HTML_FULL_RAW[$(echo ${HTML_FULL_RAW} | cut -c 1-80) ...]"
        else
            exit_with_error_message "load_raw_html can't read CACHE_FILE[$CACHE_FILE]."
        fi
    else
        exit_with_error_message "load_raw_html need reservoir id RESERVOIR_ID[$RESERVOIR_ID] not defined."

    fi
}

function set_reservoir_id {
    if [[ -z "$1" ]] ;
    then
        exit_with_error_message "set_reservoir_id need reservoir id as parameter 1[$1] not defined."
    else
        if [[ $(jq --arg key "$1" 'has($key)' "$JSON_RESERVOIRS_IDS_PATH" | tr '[:upper:]' '[:lower:]') == true ]] ;
        then
            log_write_debug "set_reservoir_id find provied id 1[$1] in json JSON_RESERVOIRS_IDS_PATH[$( basename $JSON_RESERVOIRS_IDS_PATH)]."
            RESERVOIR_ID="$1"
            log_write_info "Setted RESERVOIR_ID[${RESERVOIR_ID}]"
        else
            exit_with_error_message "set_reservoir_id provided id[$1] not exists in json JSON_RESERVOIRS_IDS_PATH[$JSON_RESERVOIRS_IDS_PATH]."
        fi
    fi

    }

function set_reservoir_relative_url_form_id {
    local id="$RESERVOIR_ID"

    if [[ ! -z "$id" ]];
    then
        local reservoir_relative_url=$(jq ".\"$id\"" --raw-output "$JSON_RESERVOIRS_IDS_PATH")
        if [[ ! -z "$reservoir_relative_url" ]] || [[ ! "$reservoir_relative_url" == "null" ]];
        then
            RESERVOIR_URL_RELATIVE="$reservoir_relative_url"
            log_write_debug "Setted RESERVOIR_URL_RELATIVE[${RESERVOIR_URL_RELATIVE}]"
        fi
    else
        exit_with_error_message "set_reservoir_relative_url_form_id can't find id, RESERVOIR_ID[$RESERVOIR_ID] not defined."
    fi
}


#################################################################################
# Process data
#################################################################################

function set_message_in_html {
    if [[ -z "$RESERVOIR_ID" ]];
    then
        exit_with_error_message "set_message_in_html need RESERVOIR_ID[$RESERVOIR_ID] not defined."
        exit
    else
        define_json_reservoir_from_raw_html "$RESERVOIR_ID"
    fi

    if [[ ! -z "$JSON_RESERVOIR" ]];
    then
        local message="$(json_message_to_html)"
        if [ ! -z "$message" ];
        then
            MESSAGE_IN_HTML=$(printf "%s\n" "$message")
            log_write_debug "set_message_in_html printed message[$(echo ${message} | cut -c 1-80) ...]"
        fi
    else
        exit_with_error_message "JSON_RESERVOIR not defined [$JSON_RESERVOIR]"
    fi
}

function send_telegram_reservoir_message_html {
    if [[ -z "$RESERVOIR_ID" ]];
    then
        exit_with_error_message "send_telegram_reservoir_message_html need reservoir id RESERVOIR_ID[$RESERVOIR_ID] not defined."
        exit
    fi
    set_message_in_html
    telegram_send_html_message
    log_write_ok "Sended telegram html message [$(echo ${reservoir_message_html} | cut -c 1-80) ...]"
}

function print_reservoir_message_txt {
    if [[ -z "$RESERVOIR_ID" ]];
    then
        exit_with_error_message "print_reservoir_message_txt need RESERVOIR_ID[$RESERVOIR_ID] not defined."
        exit
    else
        define_json_reservoir_from_raw_html "$RESERVOIR_ID"
    fi

    if [[ ! -z "$JSON_RESERVOIR" ]];
    then
        local message="$(json_message_to_txt)"
        if [ ! -z "$message" ];
        then
            printf "%s\n" "$message"
            log_write_debug "print_reservoir_message_txt printed message[$(echo ${message} | cut -c 1-80) ...]"
        fi
    else
        exit_with_error_message "JSON_RESERVOIR not defined [$JSON_RESERVOIR]"
    fi
}

function print_loaded_vars {
# RAW list generated with:
# grep -hE "=" $(find . -type f -iregex ".*\(\\.sh\|cfg\).*") | sed -E "s/^\s+//g" | cut -d"=" -f1| grep -E '[A-Z]' | sort | uniq | grep -v " "
local dump_message=$(
cat <<EOF
---------------------------------------
Loaded vars:
---------------------------------------
  BOT_TOKEN=[$BOT_TOKEN]
  CACHE_DIR=[$CACHE_DIR]
  CACHE_FILE=[$CACHE_FILE]
  CACHE_FILE_BASE=[$CACHE_FILE_BASE]
  CHAT_ID=[$CHAT_ID]
  CONFIG_CUSTOM_PATH=[$CONFIG_CUSTOM_PATH]
  DATE_PRETTY_FORMAT=[$DATE_PRETTY_FORMAT]
  DEBUG_VERBOSE_LEVEL=[$DEBUG_VERBOSE_LEVEL]
  DISABLE_NOTIFICATION=[$DISABLE_NOTIFICATION]
  DISABLE_WEB_PAGE_PREVIEW=[$DISABLE_WEB_PAGE_PREVIEW]
  E_DEBUG=[$E_DEBUG]
  E_ERROR=[$E_ERROR]
  E_INFO=[$E_INFO]
  E_NC=[$E_NC]
  E_OK=[$E_OK]
  FQDN=[$FQDN]
  INFO_VERBOSE_LEVEL=[$INFO_VERBOSE_LEVEL]
  JSON_RESERVOIRS_IDS_PATH=[$JSON_RESERVOIRS_IDS_PATH]
  LIB_PATH=[$LIB_PATH]
  LOG_FILE_NAME=[$LOG_FILE_NAME]
  LOG_PATH=[$LOG_PATH]
  OK_VERBOSE_LEVEL=[$OK_VERBOSE_LEVEL]
  PARSE_MODE=[$PARSE_MODE]
  PROTECT_CONTENT=[$PROTECT_CONTENT]
  RESERVOIR_ID=[$RESERVOIR_ID]
  RESERVOIR_URL_FULL=[$RESERVOIR_URL_FULL]
  RESERVOIR_URL_RELATIVE=[$RESERVOIR_URL_RELATIVE]
  SCRIPT_BASE_PATH=[$SCRIPT_BASE_PATH]
  SCRIPT_LIB_BASE_PATH=[$SCRIPT_LIB_BASE_PATH]
  SILENT_MODE=[$SILENT_MODE]
  TELEGRAM_CONFIG_FILE=[$TELEGRAM_CONFIG_FILE]
  TELEGRAM_LINE_MAX_CHARS=[$TELEGRAM_LINE_MAX_CHARS]
  TXT_LINE_MAX_CHARS=[$TXT_LINE_MAX_CHARS]
  URL=[$URL]
  URL_BASE=[$URL_BASE]
  URL_DATE=[$URL_DATE]
  USER_AGENT=[$USER_AGENT]
  USER_RESERVOIR_ID=[$USER_RESERVOIR_ID]
  VERBOSE_LEVEL=[$VERBOSE_LEVEL]
  VERSION=[$VERSION]
---------------------------------------
Long vars shortened (execute with bash -x to dump all vars content):
---------------------------------------
  HTML_FULL_RAW=[$(echo ${HTML_FULL_RAW} | cut -c 1-80) ...]
  CURL_DATA=[$(echo ${CURL_DATA} | cut -c 1-80) ...]
  JSON_RAW=[$(echo ${JSON_RAW} | cut -c 1-80) ...]
  JSON_RESERVOIR=[$(echo ${JSON_RESERVOIR} | cut -c 1-80) ...]
  MESSAGE=[$(echo ${MESSAGE} | cut -c 1-80) ...]
  MESSAGE_IN_HTML=[$(echo ${MESSAGE_IN_HTML} | cut -c 1-80) ...]
  MESSAGE_RAW=[$(echo ${MESSAGE_RAW} | cut -c 1-80) ...]
  MESSAGE_SCAPED=[$(echo ${MESSAGE_SCAPED} | cut -c 1-80) ...]
---------------------------------------
Loaded libraries:
---------------------------------------
  LIBRARY_FILES[@]=[${LIBRARY_FILES[@]}]

  OPTIONAL_LIBRARY_FILES[@]=[${OPTIONAL_LIBRARY_FILES[@]}]
EOF
)

printf "%s\n" "$dump_message" | awk -F '=' '{printf "%-30s%s\n", $1, $2}'
}

function print_version_info {
cat  <<EOM
$(echo -e $E_INFO)$BASH_SOURCE $VERSION$(echo -e $E_NC)

EOM
}
#################################################################################
# User interaction.
#################################################################################
source "$LIB_PATH/toolbox/_process_parameters.sh"
# Call to function with parameter provied by the user:
process_parameters "$@"
#################################################################################
# End of script
#################################################################################
# +info bash SECONDS: https://www.gnu.org/savannah-checkouts/gnu/bash/manual/bash.html#index-SECONDS
log_write_debug "End execution [$0] (total execution time seconds [$SECONDS] in minutes [$(( $SECONDS / 60))])"
