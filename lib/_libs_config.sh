declare -A LIBRARY_FILES
LIBRARY_FILES[bash_scripts_toolbox]="$LIB_PATH/toolbox/_bash_scripts_toolbox.sh"
LIBRARY_FILES[raw_html_to_json]="$LIB_PATH/html_json/_raw_html_to_json.sh"
LIBRARY_FILES[json_to_html]="$LIB_PATH/html_json/_json_to_html.sh"
LIBRARY_FILES[json_to_txt]="$LIB_PATH/html_json/_json_to_txt.sh"

declare -A OPTIONAL_LIBRARY_FILES
# Required only for Telegram functions:
OPTIONAL_LIBRARY_FILES[telegram_send_message]="$LIB_PATH/telegram/_send_message.sh"
