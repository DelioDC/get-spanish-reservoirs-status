################################################################################
# This html is te compatible with telegram format.
################################################################################
# DOC printf Format output codes:
#    https://www.gnu.org/software/libc/manual/html_node/Table-of-Output-Conversions.html
################################################################################
TELEGRAM_LINE_MAX_CHARS=20

function get_separator_line_html {
  # $1: character
  # $2: n times to repeat.
  local max_chars=$TELEGRAM_LINE_MAX_CHARS

  for ((i = 1; i <= $max_chars; i++)); do
    printf "%s" "$1"
  done
  printf "\n"
}


function jq_get_value_html {
  printf '%s' "$1" | jq -r "$2"
}

function json_message_to_html_header {
  # Generates the html message header.
  ##############################################################################
  if [[ -z "$1" ]] ;
  then
    echo "ERROR: [$0] json_message_to_html_header check provided parameter [$1]"
    exit 1
  else
    json="$1"
  fi

  # jq queries
  local jq_get_reservoir_name='.reservoir.info.name'
  local jq_get_reservoir_id='.reservoir.info.id'

  local reservoir_id=$(printf '%s' "$json"                | jq -r "$jq_get_reservoir_id")
  local reservoir_name=$(printf '%s' "$json"              | jq -r "$jq_get_reservoir_name")

  # 20 chars per line is prefered for don't break lines in mobile message display:
printf '
🚰   Estado do encoro <b>%s</b>  🚰
%s
' "$reservoir_name" \
  "$(get_separator_line_html '─' )"
}

function json_message_to_html_footer {
  # Generates the html message footer.
  ##############################################################################
  local reservoir_fullurl=$(         jq_get_value_html "$json" '.reservoir.info.url_full')
  local reservoir_chart_image_url=$( jq_get_value_html "$json" '.reservoir.info.chart_image.url' )
  # emables.net:
  local reservoir_domain_name=$(echo "$reservoir_fullurl" | cut -d"/" -f-3 | sed -E 's;http[s]?://www\.;;')
  printf '
%s
Fontes: <a href="%s">%s</a>, <a href="%s">%s</a>, <a href="%s">%s</a>.
' "$(get_separator_line_html '─' )" \
  "$reservoir_chart_image_url" "Gráfica anual" \
  "$reservoir_fullurl" "$reservoir_domain_name" \
  "https://www.miteco.gob.es/gl/agua/temas/evaluacion-de-los-recursos-hidricos/boletin-hidrologico.html" "Boletín hidrolóxico semanal"
}

function json_message_to_html_body {
    ##############################################################################
    # Parases provided json data from selected day and returns the formated html
    # code.
    # Parameters:
    #   1: JSON to parse.
    ##############################################################################
    # Check parameters data:
    if [[ -z "$1" ]] ;
    then
      echo "ERROR: [$0] json_message_to_html check provided parameters [$1]"
      exit 1
    fi
    JSON_RAW="$1"

    if ! json=$(printf "%s\n" "$JSON_RAW" | jq '.') ;
    then
      echo "ERROR: [$0] jq can't parse json."
      exit 1
    fi

    local reservoir_data_update_date=$(               jq_get_value_html "$json" '.data_updated_at' )

    local reservoir_filled_value=$(                   jq_get_value_html "$json" '.reservoir.filled.value' )
    local reservoir_filled_unit=$(                    jq_get_value_html "$json" '.reservoir.filled.unit' )
    local reservoir_filled_percent=$(                 jq_get_value_html "$json" '.reservoir.filled.percent' )
    local reservoir_capacity_value=$(                 jq_get_value_html "$json" '.reservoir.info.capacity.value' )
    local reservoir_capacity_unit=$(                  jq_get_value_html "$json" '.reservoir.info.capacity.unit' )

    local reservoir_variation_last_week_value=$(      jq_get_value_html "$json" '.reservoir.variation_last_week.value' )
    local reservoir_variation_last_week_unit=$(       jq_get_value_html "$json" '.reservoir.variation_last_week.unit' )
    local reservoir_variation_last_week_percent=$(    jq_get_value_html "$json" '.reservoir.variation_last_week.percent' )

    local reservoir_last_year_year=$(                 jq_get_value_html "$json" '.reservoir.last_year.year' )
    local reservoir_last_year_filled_value=$(         jq_get_value_html "$json" '.reservoir.last_year.filled' )
    local reservoir_last_year_unit=$(                 jq_get_value_html "$json" '.reservoir.last_year.unit' )
    local reservoir_last_year_percent=$(              jq_get_value_html "$json" '.reservoir.last_year.percent' )

    local reservoir_10_year_average_value=$(          jq_get_value_html "$json" '.reservoir.ten_year_average.value' )
    local reservoir_10_year_average_unit=$(           jq_get_value_html "$json" '.reservoir.ten_year_average.unit' )
    local reservoir_10_year_average_percent=$(        jq_get_value_html "$json" '.reservoir.ten_year_average.percent' )

    printf '
Capacidade total: %3i<i>%4s</i>
Auga embalsada a %s:
<b>%8i<i>%4s</i></b> o <b>%4s</b>
Variación coa semán anterior:
<b>%8i<i>%4s</i></b> un %4s
Mesma semán (%s):
<i>%8i%4s o %s</i>
Mesma semán (Med. 10 Anos):
<i>%8i%4s o %s</i>
' \
    "$reservoir_capacity_value" "$reservoir_capacity_unit" \
    "$reservoir_data_update_date" \
    "$reservoir_filled_value" "$reservoir_filled_unit" "$reservoir_filled_percent" \
    "$reservoir_variation_last_week_value" "$reservoir_variation_last_week_unit" "$reservoir_variation_last_week_percent" \
    "$reservoir_last_year" "$reservoir_last_year_filled_value" "$reservoir_last_year_unit" "$reservoir_last_year_percent" \
    "$reservoir_10_year_average" "$reservoir_10_year_average_unit" "$reservoir_10_year_average_percent"
}

function json_message_to_html {
# Check parameters data:
    if [[ -z "$JSON_RESERVOIR" ]] ;
    then
      echo "ERROR: [$0] json_message_to_html need json to parse check JSON_RESERVOIR[$JSON_RESERVOIR]"
      exit 1
    fi
    local json="$JSON_RESERVOIR"

    if ! json=$(printf "%s\n" "$JSON_RESERVOIR" | jq '.') ;
    then
      echo "ERROR: [$0] jq can't parse json."
      exit 1
    fi
  local message_html=$(json_message_to_html_header "$json")
  message_html+=$(json_message_to_html_body "$json")
  message_html+=$(json_message_to_html_footer "$json")

  echo -e "$message_html"
}
