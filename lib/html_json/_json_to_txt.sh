################################################################################
# This html is te compatible with telegram format.
################################################################################
# DOC printf Format output codes:
#    https://www.gnu.org/software/libc/manual/html_node/Table-of-Output-Conversions.html
################################################################################
TXT_LINE_MAX_CHARS=60

function get_separator_line_txt {
  # $1: character
  if [[ -z "$1" ]] ;
  then
    echo "ERROR: [$0] get_separator_line_txt check provided parameter [$1]"
    exit 1
  fi

  local max_chars=$TXT_LINE_MAX_CHARS
  for ((i = 1; i <= $max_chars; i++)); do
    printf "%s" "$1"
  done
  printf "\n"
}

function jq_get_value_for_txt {
  printf '%s' "$1" | jq -r "$2"
}

function json_message_to_txt_header {
  # Generates the txt message header.

  if [[ -z "$1" ]] ;
  then
    echo "ERROR: [$0] json_message_to_txt_header check provided parameter [$1]"
    exit 1
  else
    json="$1"
  fi

  # jq queries
  local jq_get_reservoir_name='.reservoir.info.name'
  local jq_get_reservoir_id='.reservoir.info.id'

  local reservoir_id=$(printf '%s' "$json"                | jq -r "$jq_get_reservoir_id")
  local reservoir_name=$(printf '%s' "$json"              | jq -r "$jq_get_reservoir_name")

printf '%s
Estado do encoro %s:
%s
' "$(get_separator_line_txt '─' )" \
  "$reservoir_name" \
  "$(get_separator_line_txt '─' )"
}

function json_message_to_txt_footer {
  # Generates the txt message footer.

  local reservoir_fullurl=$(         jq_get_value_for_txt "$json" '.reservoir.info.url_full')
  local reservoir_chart_image_url=$( jq_get_value_for_txt "$json" '.reservoir.info.chart_image.url' )
  # emables.net:
  local reservoir_domain_name=$(echo "$reservoir_fullurl" | cut -d"/" -f-3 | sed -E 's;http[s]?://www\.;;')
  printf '
%s
Fontes:
%s;%s
%s;%s
%s;%s
' "$(get_separator_line_txt '─' )" \
  "Gráfica anual" "$reservoir_chart_image_url" \
  "$reservoir_domain_name" "$reservoir_fullurl" \
  "Boletín hidrolóxico semanal" "https://www.miteco.gob.es/gl/agua/temas/evaluacion-de-los-recursos-hidricos/boletin-hidrologico.html"
}

function json_message_to_txt_body {
    ##############################################################################
    # Parases provided json data from selected day and returns the formated txt.
    # Parameters:
    #   1: JSON to parse.
    ##############################################################################
    # Check parameters data:
    if [[ -z "$1" ]] ;
    then
      echo "ERROR: [$0] json_message_to_txt_body check provided parameters [$1]"
      exit 1
    fi
    JSON_RAW="$1"

    if ! json=$(printf "%s\n" "$JSON_RAW" | jq '.') ;
    then
      echo "ERROR: [$0] jq can't parse json."
      exit 1
    fi

    local reservoir_data_update_date=$(               jq_get_value_for_txt "$json" '.data_updated_at' )

    local reservoir_filled_value=$(                   jq_get_value_for_txt "$json" '.reservoir.filled.value' )
    local reservoir_filled_unit=$(                    jq_get_value_for_txt "$json" '.reservoir.filled.unit' )
    local reservoir_filled_percent=$(                 jq_get_value_for_txt "$json" '.reservoir.filled.percent' )
    local reservoir_capacity_value=$(                 jq_get_value_for_txt "$json" '.reservoir.info.capacity.value' )
    local reservoir_capacity_unit=$(                  jq_get_value_for_txt "$json" '.reservoir.info.capacity.unit' )

    local reservoir_variation_last_week_value=$(      jq_get_value_for_txt "$json" '.reservoir.variation_last_week.value' )
    local reservoir_variation_last_week_unit=$(       jq_get_value_for_txt "$json" '.reservoir.variation_last_week.unit' )
    local reservoir_variation_last_week_percent=$(    jq_get_value_for_txt "$json" '.reservoir.variation_last_week.percent' )

    local reservoir_last_year_year=$(                 jq_get_value_for_txt "$json" '.reservoir.last_year.year' )
    local reservoir_last_year_filled_value=$(         jq_get_value_for_txt "$json" '.reservoir.last_year.filled' )
    local reservoir_last_year_unit=$(                 jq_get_value_for_txt "$json" '.reservoir.last_year.unit' )
    local reservoir_last_year_percent=$(              jq_get_value_for_txt "$json" '.reservoir.last_year.percent' )

    local reservoir_10_year_average_value=$(          jq_get_value_for_txt "$json" '.reservoir.ten_year_average.value' )
    local reservoir_10_year_average_unit=$(           jq_get_value_for_txt "$json" '.reservoir.ten_year_average.unit' )
    local reservoir_10_year_average_percent=$(        jq_get_value_for_txt "$json" '.reservoir.ten_year_average.percent' )

    # The character ";" is used by column command, to recognice the columns:
    printf '
Capacidade total:;%i%s;100%%
Auga embalsada a %s:;%i%s;%s
Variación coa semán anterior:;%i%s;%s
Mesma semán (%s):;%i%s;%s
Mesma semán (Med. 10 Anos):;%i%s;%s
' \
    "$reservoir_capacity_value" "$reservoir_capacity_unit" \
    "$reservoir_data_update_date" \
    "$reservoir_filled_value" "$reservoir_filled_unit" "$reservoir_filled_percent" \
    "$reservoir_variation_last_week_value" "$reservoir_variation_last_week_unit" "$reservoir_variation_last_week_percent" \
    "$reservoir_last_year" "$reservoir_last_year_filled_value" "$reservoir_last_year_unit" "$reservoir_last_year_percent" \
    "$reservoir_10_year_average" "$reservoir_10_year_average_unit" "$reservoir_10_year_average_percent"
}

function json_message_to_txt {
    if [[ -z "$JSON_RESERVOIR" ]] ;
    then
      echo "ERROR: [$0] json_message_to_txt need json to parse check JSON_RESERVOIR[$JSON_RESERVOIR]"
      exit 1
    fi

    local json="$JSON_RESERVOIR"
    if ! json=$(printf "%s\n" "$JSON_RESERVOIR" | jq '.') ;
    then
      echo "ERROR: [$0] jq can't parse json."
      exit 1
    fi

  local message_txt=$(json_message_to_txt_header "$json")
  message_txt+=$(json_message_to_txt_body "$json")
  message_txt+=$(json_message_to_txt_footer "$json")

  echo -e "$message_txt" | awk -F';' '
    {
        gsub(/^[ \t]+|[ \t]+$/, "", $2);
        if ($0 !~ /;/) {
            print $0;
        } else {
            if (!$3) {
                printf "%-30s:  %-6s\n", $1, $2;
                next;
            }
            if ($2 ~ /^-?[0-9]+hm3$/) {
                printf "%-30s %-6s %6s\n", $1, $2, $3;
            }else {
                printf "%-30s %-6s %-6s\n", $1, $2, $3;
            }
        }
    }
'
}

