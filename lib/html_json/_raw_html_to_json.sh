# Parse raw html to get a json to work.
function define_json_reservoir_from_raw_html {
    ################################################################################
    # DESCRIPTION: Process the original html code with xmllint to extract the data,
    #              generates a well structured json with the data using jq.
    ################################################################################
    # REQUIRES: xmllint, jq
    ################################################################################
    if [[ -z "$1" ]] ;
    then
        exit_with_error_message "define_json_reservoir_from_raw_html need reservoir id as parameter 1[$1] not defined."
    else
        if [[ $(jq --arg key "$1" 'has($key)' "$JSON_RESERVOIRS_IDS_PATH" | tr '[:upper:]' '[:lower:]') == true ]] ;
        then
#             log_write_debug "define_json_reservoir_from_raw_html find provied id in first parameter [$1] in json JSON_RESERVOIRS_IDS_PATH[$( basename $JSON_RESERVOIRS_IDS_PATH)]."
            RESERVOIR_ID="$1"
#             log_write_info "Setted RESERVOIR_ID[${RESERVOIR_ID}]"
            load_raw_html
        else
            exit_with_error_message "define_json_reservoir_from_raw_html provided id[$1] not exists in json JSON_RESERVOIRS_IDS_PATH[$JSON_RESERVOIRS_IDS_PATH]."
        fi
    fi

#     log_write_debug "define_json_reservoir_from_raw_html is calling xmllint to generate json vars"
    ################################################################################
    # html node with target contents:
    ################################################################################
    # Parses original raw html of first div with class="SeccionCentral", to generate
    # the specific html to work along the function.
    # From xmllint help:
    #   --nowarning Do not emit warnings from the parser and/or validator.
    #   --nowrap Do not output HTML doc wrapper.
    #   --nocdata Substitute CDATA section by equivalent text nodes.
    #   --noent Substitute entity values for entity references. By default, xmllint leaves entity references in place.
    #   --nonet Do not use the Internet to fetch DTDs or entities.
    #   --noblanks Drop ignorable blank spaces.
    ################################################################################
    xmllint_output=$(printf "%s" "$HTML_FULL_RAW" \
                    | xmllint --html --nowarning --nowrap --nocdata --noent --nonet \
                              --noblanks --xpath "/html/body/section//div[@class='SeccionCentral'][1]/div/*" - 2> /dev/null
                    )
    # If you don't remove the original source windows breakline characters, you must found in the output of xmllint the entity "&#13;"
    # in this case, you can remove the html entities with:
    #| sed 's/\&\#13\;//' \

    # "Little hack" forces xmllint to work with utf-8 this prevent outputs with extrange characters:
    xmllint_output=$(printf "<meta charset='utf8'>%s" "$HTML_FULL_RAW")

    ################################################################################
    # Getting clean data from raw html:
    ################################################################################
    # 1º row in raw data "Agua embalsada (DD-MM-YYYY)" (current info):
    reservoir_name=$(printf "%s" "$xmllint_output" \
                    | xmllint --html -xpath "(//div[@class='SeccionCentral_TituloTexto'])[1]/text()" - 2> /dev/null \
                    | sed -E 's/^.+:\s*(.+)$/\1/' \
                    )
    reservoir_data_update_date=$(printf "%s" "$xmllint_output" \
                                | xmllint --html -xpath "//div[@class='FilaSeccion'][1]/div[@class='Campo']//text()" - 2> /dev/null \
                                | sed -E 's/^.+:\s*(.+)$/\1/' \
                                | sed -nE 's/^[^0-9]+([0-9]+)-([0-9]+)-([0-9]+).*$/\3\/\2\/\1/p' \
                                )
    reservoir_data_update_date=$( date_prettify "$reservoir_data_update_date" )
    reservoir_filled=$(printf "%s" "$xmllint_output" \
                     | xmllint --html -xpath "//div[@class='FilaSeccion'][1]/div[@class='Resultado'][1]//text()" - 2> /dev/null \
                     )
    # Current filled unit (compose by 2 nodes, remove the linebreak):
    reservoir_filled_unit=$(printf "%s" "$xmllint_output" \
                           | xmllint --html -xpath "//div[@class='FilaSeccion'][1]/div[@class='Unidad']//text()" - 2> /dev/null \
                           | tr -d '\n' \
                           )
    # Current percent value (append character % at end):
    reservoir_filled_percent=$(printf "%s" "$xmllint_output" \
                             | xmllint --html -xpath "//div[@class='FilaSeccion'][1]/div[@class='Resultado'][2]//text()" - 2> /dev/null \
                             | sed -E 's/$/%/'
                             )
    # 2º row in raw data "Variacion semana Anterior" (last week variation):
    reservoir_variation_last_week_value=$(printf "%s" "$xmllint_output" \
                                         | xmllint --html -xpath "//div[@class='FilaSeccion'][2]/div[@class='Resultado'][1]//text()" - 2> /dev/null \
                                         )
    reservoir_variation_last_week_unit=$(printf "%s" "$xmllint_output" \
                                        | xmllint --html -xpath "//div[@class='FilaSeccion'][2]/div[@class='Unidad']//text()" - 2> /dev/null \
                                        | tr -d '\n' \
                                        )
    reservoir_variation_last_week_percent=$(printf "%s" "$xmllint_output" \
                                           | xmllint --html -xpath "//div[@class='FilaSeccion'][2]/div[@class='Resultado'][2]//text()" - 2> /dev/null \
                                           | sed -E 's/$/%/'
                                           )
    # 3º row in raw data "Capacidad":
    reservoir_capacity=$(printf "%s" "$xmllint_output" \
                        | xmllint --html -xpath "//div[@class='FilaSeccion'][3]/div[@class='Resultado'][1]//text()" - 2> /dev/null \
                        )
    reservoir_capacity_unit=$(printf "%s" "$xmllint_output" \
                            | xmllint --html -xpath "//div[@class='FilaSeccion'][2]/div[@class='Unidad']//text()" - 2> /dev/null \
                            | tr -d '\n' \
                            )
    # 4º row in raw data "Misma Semana (YYYY)":
    reservoir_last_year=$(printf "%s" "$xmllint_output" \
                         | xmllint --html -xpath "//div[@class='FilaSeccion'][4]/div[@class='Campo']//text()" - 2> /dev/null \
                         | sed -E 's/^[^0-9]+([0-9+]+)[^0-9]+$/\1/'
                         )
    reservoir_last_year_filled=$(printf "%s" "$xmllint_output" \
                                | xmllint --html -xpath "//div[@class='FilaSeccion'][4]/div[@class='Resultado'][1]//text()" - 2> /dev/null
                                )
    reservoir_last_year_unit=$(printf "%s" "$xmllint_output" \
                              | xmllint --html -xpath "//div[@class='FilaSeccion'][4]/div[@class='Unidad']//text()" - 2> /dev/null \
                              | tr -d '\n'
                              )
    reservoir_last_year_percent=$(printf "%s" "$xmllint_output" \
                                 | xmllint --html -xpath "//div[@class='FilaSeccion'][4]/div[@class='Resultado'][2]//text()" - 2> /dev/null \
                                 | sed -E 's/$/%/'
                                 )
    # 5º row in raw data "Misma Semana (Med. 10 Años)":
    reservoir_10_year_average=$(printf "%s" "$xmllint_output" \
                               | xmllint --html -xpath "//div[@class='FilaSeccion'][5]/div[@class='Resultado'][1]//text()" - 2> /dev/null
                               )
    reservoir_10_year_average_unit=$(printf "%s" "$xmllint_output" \
                                    | xmllint --html -xpath "//div[@class='FilaSeccion'][5]/div[@class='Unidad'][1]//text()" - 2> /dev/null \
                                    | tr -d '\n'
                                    )
    reservoir_10_year_average_percent=$(printf "%s" "$xmllint_output" \
                                       | xmllint --html -xpath "//div[@class='FilaSeccion'][5]/div[@class='Resultado'][2]//text()" - 2> /dev/null \
                                       | sed -E 's/$/%/'
                                       )
    # 6º row in raw data "Gráfico" (annual chart with weeks data of the last 3 years)
    reservoir_chart_image_url=$(printf "%s" "$xmllint_output" \
                               | xmllint --html -xpath "string(//div[@class='Grafico'][1]/img/@src)" - 2> /dev/null
                               )
    reservoir_chart_image_url="$FQDN$reservoir_chart_image_url"
    reservoir_chart_description=$(printf "%s" "$xmllint_output" \
                                 | xmllint --html -xpath "string(//div[@class='Grafico'][1]/img/@title)" - 2> /dev/null \
                                 | sed -E 's/^Pantano Eiras[\.\ ]+//'
                                 )
    ################################################################################
    # Generating json with jq:
    ################################################################################

#     log_write_debug "define_json_reservoir_from_raw_html is generating JSON_RESERVOIR content..."
    JSON_RESERVOIR=$(jq -n \
                    --arg reservoir_data_update_date "$reservoir_data_update_date" \
                    --arg reservoir_name "$reservoir_name" \
                    --arg reservoir_id "$USER_RESERVOIR_ID" \
                    --arg reservoir_url_relative "$RESERVOIR_URL_RELATIVE" \
                    --arg reservoir_url_full "$RESERVOIR_URL_FULL" \
                    --arg reservoir_capacity "$reservoir_capacity" \
                    --arg reservoir_capacity_unit "$reservoir_capacity_unit" \
                    --arg reservoir_filled "$reservoir_filled" \
                    --arg reservoir_filled_unit "$reservoir_filled_unit" \
                    --arg reservoir_filled_percent "$reservoir_filled_percent" \
                    --arg reservoir_variation_last_week_value "$reservoir_variation_last_week_value" \
                    --arg reservoir_variation_last_week_unit "$reservoir_variation_last_week_unit" \
                    --arg reservoir_variation_last_week_percent "$reservoir_variation_last_week_percent" \
                    --arg reservoir_last_year "$reservoir_last_year" \
                    --arg reservoir_last_year_filled "$reservoir_last_year_filled" \
                    --arg reservoir_last_year_unit "$reservoir_last_year_unit" \
                    --arg reservoir_last_year_percent "$reservoir_last_year_percent" \
                    --arg reservoir_10_year_average "$reservoir_10_year_average" \
                    --arg reservoir_10_year_average_unit "$reservoir_10_year_average_unit" \
                    --arg reservoir_10_year_average_percent "$reservoir_10_year_average_percent" \
                    --arg reservoir_chart_image_url "$reservoir_chart_image_url" \
                    --arg reservoir_chart_description "$reservoir_chart_description" \
                    '{
                        data_updated_at: $reservoir_data_update_date,
                        reservoir: {
                        info: {
                            id: $reservoir_id,
                            name: $reservoir_name,
                            url_relative: $reservoir_url_relative,
                            url_full: $reservoir_url_full,
                            capacity: {
                            value: $reservoir_capacity,
                            unit: $reservoir_capacity_unit
                            },
                            chart_image: {
                            url: $reservoir_chart_image_url,
                            description: $reservoir_chart_description
                            }
                        },
                        filled: {
                            value: $reservoir_filled,
                            unit: $reservoir_filled_unit,
                            percent: $reservoir_filled_percent
                        },
                        variation_last_week: {
                            value: $reservoir_variation_last_week_value,
                            unit: $reservoir_variation_last_week_unit,
                            percent: $reservoir_variation_last_week_percent
                        },
                        last_year: {
                            year: $reservoir_last_year,
                            filled: $reservoir_last_year_filled,
                            unit: $reservoir_last_year_unit,
                            percent: $reservoir_last_year_percent
                        },
                        "ten_year_average": {
                            value: $reservoir_10_year_average,
                            unit: $reservoir_10_year_average_unit,
                            percent: $reservoir_10_year_average_percent
                        }
                        }
                    }'
                    )
#     log_write_debug "Setted JSON_RESERVOIR[$(echo ${JSON_RESERVOIR} | cut -c 1-80) ...]"

    if [[ -z "$JSON_RESERVOIR" ]];
    then
        exit_with_error_message "define_json_reservoir_from_raw_html can't define JSON_RESERVOIR[$(echo ${JSON_RESERVOIR} | cut -c 1-80) ...]"

    fi
}
