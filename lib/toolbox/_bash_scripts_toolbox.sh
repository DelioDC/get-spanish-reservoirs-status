################################################################################
# Author: DelioDC
# ! This is a unofficial script, use only for test purposes.
################################################################################
# License:
################################################################################
# Copyright 2023 Delio Docampo Cordeiro
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#   http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
################################################################################
# Verbose levels: 0 OK, 1 INFO, 3 DEBUG (2 WARN, not used)
################################################################################
SCRIPT_LIB_BASE_PATH=$(cd $(dirname "${BASH_SOURCE[0]}") && pwd)
# Colors for echo -e:
# +info: https://web.archive.org/web/20230320174225/https://www.shellhacks.com/bash-colors/
E_ERROR='\e[1;31m'   # Bold, red
E_WARN='\e[93m' # Yellow Light
E_OK='\e[0;32m'    # Green
OK_VERBOSE_LEVEL=0

E_INFO='\e[0;34m'    # Blue
INFO_VERBOSE_LEVEL=1

E_DEBUG='\e[0;33m'   # Yellow
DEBUG_VERBOSE_LEVEL=3

E_NC='\033[0m'       # No color

DATE_PRETTY_FORMAT='+%d/%m/%Y'

function get_log_timestamp {
    echo -e "$E_NC[$(date +%Y-%m-%d_%H:%M:%S)]$E_NC"
}

function echo_error_message {
    ####################################
    # Description:
    #  Prints a error message in &2 with color of $E_ERROR
    ####################################
    # Params:
    #  - Error message
    ####################################
    >&2 echo -e  $E_ERROR"ERROR: $1"$E_NC
}

function exit_with_error_message {
    ####################################
    # Description:
    #  Prints error message and exits with defined return code
    ####################################
    if [[ ! -z "$1" ]]; then
#         echo_error_message "$1"
#         log_write "ERROR" "$1"
        log_write_error "$1"
        exit 2
    else
        log_write_error "function exit_with_error_message needs 1 parameter"
#         log_write "ERROR" "function exit_with_error_message needs 1 parameter"
        exit 1
    fi
}
function log_write_line {
    local line_type="$1"
    local message="$2"
    if [[ ! -r "$LOG_PATH" ]]; then
        echo -e "$(get_log_timestamp)[${E_INFO}INFO${E_NC}] Previous log file not found, created this log file in [$LOG_PATH]" >> "$LOG_PATH"
    fi

    if [[ -w "$LOG_PATH" ]]; then
        if [[ ! -z "$line_type" && ! -z "$message" ]]; then
        echo -e "$(get_log_timestamp)$E_NC[$line_type${E_NC}] $message" >> "$LOG_PATH"
    else
        echo_error_message "log_write_line undefined parameters 1[$1] or 2[$2]"
    fi
    else
      echo_error_message "function log_write_line can't write in log file [$LOG_PATH]"
      exit 3
    fi
}

function log_write {
    # $1 = Type of log entry
    # $2 = Log message
    #########################

    if [[ ! -z "$1" && ! -z "$2" ]];
    then
        local log_line_type="$(echo $1 | tr '[:lower:]' '[:upper:]')"
        local log_line_message="$2"
        case "$log_line_type" in
          ERROR)
            log_write_line "${E_ERROR}ERROR" "${log_line_message}"
          ;;
          INFO)
            log_write_line "${E_INFO}INFO" "${log_line_message}"
          ;;
          DEBUG)
            log_write_line "${E_DEBUG}DEBUG" "${log_line_message}"
          ;;
          OK)
            log_write_line "${E_OK}OK" "${log_line_message}"
          ;;
          NORMAL|REGULAR)
            log_write_line "${E_NC}NC" "${log_line_message}"
          ;;
          *)
            log_write_line "${E_DEBUG}ERROR" "log_write can't process the log_line_type type [$log_line_type] message [$log_line_message]"
          ;;
        esac
    else
      echo_error_message "function log_write, needs 2 parameter, provided [$1] and [$2]"
    fi
}

function log_write_error {
    # Writes the provided line in $1 to the log file.
    local message="$1"
    if [[ ! -z "$message" ]]; then
        echo -e "$(get_log_timestamp) - ${E_ERROR}ERROR: $message ${E_NC}"
        log_write_line "${E_ERROR}ERROR" "${message}"
    else
        echo_error_message "function log_write_error error, needs 1 parameter, provided [$1]"
    fi
}
function log_write_ok {
    # Writes the provided line in $1 to the log file.
    local message="$1"
    if [[ ! -z "$message" ]]; then
        if [[ -z "$SILENT_MODE" ]] && [[ "$VERBOSE_LEVEL" -ge $OK_VERBOSE_LEVEL ]]; then
            echo -e "$(get_log_timestamp) - ${E_OK}OK: $message ${E_NC}"
        fi
        log_write_line "${E_OK}OK" "${message}"
    else
        echo_error_message "function log_info_error error, needs 1 parameter, provided [$1]"
    fi
}
function log_write_info {
    # Writes the provided line in $1 to the log file.
    local message="$1"
    if [[ ! -z "$message" ]]; then
        if [[ -z "$SILENT_MODE" ]] && [[ "$VERBOSE_LEVEL" -ge $INFO_VERBOSE_LEVEL ]]; then
            echo -e "$(get_log_timestamp) - ${E_INFO}INFO: $message ${E_NC}"
        fi
        log_write_line "${E_INFO}INFO" "${message}"
    else
        echo_error_message "function log_info_error error, needs 1 parameter, provided [$1]"
    fi
}
function log_write_warn {
    # Writes the provided line in $1 to the log file.
    local message="$1"
    if [[ ! -z "$message" ]]; then
        if [[ -z "$SILENT_MODE" ]] && [[ "$VERBOSE_LEVEL" -ge $DEBUG_VERBOSE_LEVEL ]]; then
            echo -e "$(get_log_timestamp) - ${E_WARN}WARN: $message ${E_NC}"
        fi
        log_write_line "${E_WARN}WARN" "${message}"
    else
        echo_error_message "function log_info_error error, needs 1 parameter, provided [$1]"
    fi
}
function log_write_debug {
    # Writes the provided line in $1 to the log file.
    local message="$1"
    if [[ ! -z "$message" ]]; then
        if [[ -z "$SILENT_MODE" ]] && [[ "$VERBOSE_LEVEL" -ge $DEBUG_VERBOSE_LEVEL ]]; then
            echo -e "$(get_log_timestamp) - ${E_DEBUG}DEBUG: $message ${E_NC}"
        fi
        log_write_line "${E_DEBUG}DEBUG" "${message}"
    else
        echo_error_message "function log_info_error error, needs 1 parameter, provided [$1]"
    fi
}

function curl_call {
    ####################################
    # Description:
    #  Returns the web response.
    ####################################
    # Parameters:
    #  - web url
    ####################################
    if [[ ! -z $USER_AGENT && ! -z $1 ]]; then
        curl  -s --user-agent "$USER_AGENT" "$1"
    else
        exit_with_error_message "curl_call parameters not defined in curl_call function call"
    fi
}

function get_redirection_url {
    ####################################
    # Description:
    #  Parses the url redirection response.
    ####################################
    if [[ ! -z "$USER_AGENT" && ! -z "$1" ]]; then
        local url_redirected=$(curl -s --user-agent "$USER_AGENT" -L "$1" | grep -Eo '(http|https):[^\"]+')
        log_write_debug "get_redirection_url result [$url_redirected]"
        echo "$url_redirected"
    fi
}

function date_prettify {
  if [[ ! -z "$0" ]];
  then
    date --date "$1" "$DATE_PRETTY_FORMAT"
    return 0
  else
    exit 1
  fi
}


