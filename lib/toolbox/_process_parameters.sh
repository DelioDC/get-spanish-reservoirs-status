function print_help {
cat << EOM
Usage: $0 [options...]

Description:
   This script requires the reservoir ID as a mandatory parameter. It offers options to interact
   with reservoir data and send messages.

Options:
 --id=<number>                  Reservoir ID (Required).
 -S, --send-telegram-message    Send HTML message to Telegram (config in telegram_bot.cfg).
 -s, --silent                   Run silently without trace lines, only text ouput like -T.
 -T, --reservoir-message-txt    Print reservoir message in text format.
 -v,-vvv                        Verbosity levels: v info, vvv debug.
 -P,--print-loaded-vars         Print a dump of loaded vars, util for debug.
 -h, --help                     Get help for commands (show this help).
 -V, --version                  Show version number and quit

Examples:
  Print data in stdout formatted as text:
    $ $0 --id=123 -T
  Send Telegram message in HTML format:
    $ $0 --id=456 -S
  Print the data and send a telegram message:
    $ $0 --id=987 -T -S
EOM
}

process_parameters() {
    if [ $# -eq 0 ]; then
        print_help
        exit_with_error_message "Paramter/s not provided in script call"
        return
    fi

    #
    # Sort user parameters by PARAMATERS_PRIORITY_REGEX order:
    #
    local -a PARAMATERS_PRIORITY_REGEX=( "-h" "--help" "-s" "--silent" "-v" "-vv" "-vvv" "-i" "--id"  "-V" "-T" "-S" "-P" )
    local reorganized_args=()

    local i=0;
    local len_parameters_priority_regexp=${#PARAMATERS_PRIORITY_REGEX[@]}

    while [[ $i -lt "$len_parameters_priority_regexp" ]];
    do
        for user_argument in "$@"; do
            if [[ "$user_argument" =~ ^"${PARAMATERS_PRIORITY_REGEX[$i]}" ]]; then
                reorganized_args+=( "$user_argument" )
                break
            fi
        done

        if [[ "${#reorganized_args[@]}" -ge "$#" ]];
        then
            # All user params are processed, skip the rest of iteractions:
            i="$len_parameters_priority_regexp"
        fi

        i=$((i+1))
    done

    # Append the rest of argument:
    for arg in "$@"; do
        if ! [[ " ${reorganized_args[@]} " =~ " ${arg} " ]]; then
            reorganized_args+=( "$arg" )
        fi
    done

    #
    # With sorted parameters by priority, we can define the reservoir id before call another functions:
    #

    for arg in "${reorganized_args[@]}"; do
        case "$arg" in
            -s | --silent)
                SILENT_MODE="true"
                ;;
            --id* | -i*)
                USER_RESERVOIR_ID=$(echo "$arg" | grep -oE '[0-9]+')
                set_reservoir_id "$USER_RESERVOIR_ID"
                ;;
            -v | -vv | -vvv)
                if [ -z "$VERBOSE_LEVEL" ]; then
                    VERBOSE_LEVEL=$(echo "$arg" | tr -cd 'v' | wc -c)
                fi
                ;;
            -S | --send-telegram-message)
                send_telegram_reservoir_message_html
                ;;
            -T | --reservoir-message-txt)
                print_reservoir_message_txt
                ;;
            -P | --print-loaded-vars)
                print_loaded_vars
                ;;
            --version | -V)
                print_version_info
                ;;
            --help | -h)
                print_help
                ;;
            *)
                print_help
                exit_with_error_message "Check script call parameters, parameter/s not recognized"
                ;;
        esac
    done
}
