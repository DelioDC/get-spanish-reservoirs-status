################################################################################
# API DOC: https://core.telegram.org/bots/api#sendmessage
################################################################################
# This library file must be relative to main script path.

if [[ ! -r "$TELEGRAM_CONFIG_FILE" ]];
then
  log_write_warn "telegram [$PWD] library [_send_message.sh] can't read cfg file TELEGRAM_CONFIG_FILE[$TELEGRAM_CONFIG_FILE] Telegram functions can't be executed"
else
  . "$TELEGRAM_CONFIG_FILE"

  if [[ -z "$BOT_TOKEN" ]] ;
  then
    exit_with_error_message "telegram library [$0] needs a valid BOT_TOKEN definition, BOT_TOKEN[$BOT_TOKEN] not defined"
  fi

  if [[ -z "$CHAT_ID" ]];
  then
    exit_with_error_message "telegram library [$0] needs a valid CHAT_ID definition, CHAT_ID[$CHAT_ID] not defined"
  fi
fi

function telegram_send_html_message {
  ##############################################################################
  # Sends message to Telegram bot sendMessage API call.
  ##############################################################################
  if [[ ! -z "$MESSAGE_IN_HTML" ]];
  then
    MESSAGE_RAW="$MESSAGE_IN_HTML"
    # Especial telegram characters in markdownv2: https://core.telegram.org/bots/api#markdownv2-style
    # _', '*', '[', ']', '(', ')', '~', '`', '>', '#', '+', '-', '=', '|', '{', '}', '.', '!'
    # In html mode we only need scape the hash character:
    #     TELEGRAM_RESERVED_CHARACTERS='\#'
    #     MESSAGE_SCAPED="$(echo -e "$MESSAGE_RAW" |  sed -r -e 's/('"${TELEGRAM_RESERVED_CHARACTERS}"')/\\\1/g' )"
    MESSAGE_SCAPED="$(echo -e "$MESSAGE_RAW")"
    # Maybe you can use the package "urlencode" or another, but in the end they end up using other lenguages.
    # https://metacpan.org/pod/URI::Escape
    MESSAGE="$(perl -MURI::Escape -e 'print uri_escape($ARGV[0]);' "$MESSAGE_SCAPED")"

    # API Telegram url for sendMessage:
    URL='https://api.telegram.org/bot'"$BOT_TOKEN"'/sendMessage'

    # Passed to curl for option "-d, --data <data>" it's similar to send a form fields:
    CURL_DATA="chat_id=$CHAT_ID&text=$MESSAGE&parse_mode=$PARSE_MODE&disable_web_page_preview=$DISABLE_WEB_PAGE_PREVIEW&disable_notification=$DISABLE_NOTIFICATION&protect_content=$PROTECT_CONTENT"

    # Send the message to telegram api:
    curl -s --data "$CURL_DATA" "$URL" --output "/dev/null"
  else
    echo "ERROR: MESSAGE_IN_HTML[$MESSAGE_IN_HTML] empty or not defined."
  fi
}
