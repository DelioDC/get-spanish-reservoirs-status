# get_embalse_status.sh - BASH CLI Application

This BASH command-line application provides summaries of the status of reservoirs in Spain.

It retrieves data from embalses.net and transforms this data into a command-line message or sends it in a HTML-formatted Telegram message.

## Features
- Retrieve reservoir status based on embalses.net reservoir ID.
- Prints TXT-formatted output status data.
- Send HTML-formatted messages to Telegram.
- Adjustable verbosity levels for debugging.
- Easy-to-use command-line interface.

## Execution example

```bash
bash get_embalse_status.sh -S -T --id=1236
```

![Example of bash execution who returned the message in text result](multimedia/images/examples/bash-example.png "Bash execution example")

![Example of generated Telegram message](multimedia/images/examples/telegram-message.png "Telegram message example")


## Installation

Install the required packages, clone the repo, configure.

### Required packages
This application was tested in Archlinux with this packages version:

|Package:              | Version: |
|:---                  |:---:     |
|coreutils (gnu tools) | 9.3-1    |
|libxml2 (xmllint)     | 2.11.5-1 |
|curl                  | 8.2.1-1  |
|jq                    | 1.6-4    |


### Configure
#### Obtain reservoir id
Check you desired reservoir to get the id.

Search in the provided json with jq (of course you can grep the .json if you like):

```
jq 'to_entries | map(select(.value | contains("<string_to_search>"))) | from_entries' lib/jsons/reservoirs-ids.json
```

Or search in the original web embalses.net, the id is in the number of the reservoir url.

With this id, you are ready to work with the default script config:
```
./get_embalse_status.sh --id=123
```
#### Setup your application user
For basic security recommendations, you should not run this script as root.

Create a user to execute this script or if you have one, use them.

#### Customize your config
To use custom config, you must copy (or rename) the config template at the root proyect dir with the extension .cfg:

```
cp -v config.cfg.template config.cfg
```

##### CACHE_DIR
By default the script creates and uses a cache dir in:

```
/home/<user>/.cache/<script_name>
```

If you want to use other path, specify the parent dir in CACHE_DIR:

```
CACHE_DIR="/my/custom/cache/dir"
```
The script goes to create a subdirectory in this path.

##### LOG_PATH_CUSTOM
By default it goes to generate a .log file by day using a timestamp:

```
<dir of main script>/get_embalse_status_<timestamp>.log
```
If you want another log path follow this instructions:

###### Create a dir for logs

As recommendation, create a new directory in:

```
/var/log/<app_log_dir>
```

###### Set the full path of .log file

When you set a full path to log file, it goes to write the log output to the same file without rotation.

```
LOG_PATH_CUSTOM="/var/log/get_embalse_status/get_embalse_status.log"
```

You must configure in your sistem something like logrotate if you like.

##### USER_AGENT

By default the scripts uses a most common Mozilla Firefox user agent.

If you want to customize the user agent used by curl, uncomment this var and set one:

```
#USER_AGENT=''
```

### Telegram configuration

See [Telegram configuration doc](docs/telegram_config.md).


## Usage

```
./get_embalse_status.sh [options...]
```

### Options

| Parameter:                   | Description: |
| :---                         | :---|
|--id=<number>                 | Reservoir ID (Required).|
|-S, --send-telegram-message   | Send HTML message to Telegram (config in telegram_bot.cfg).|
|-s, --silent                  | Run silently without trace lines, only text ouput like -T.|
|-T, --reservoir-message-txt   | Print reservoir message in text format.|
|-v,-vvv                       | Verbosity levels: v info, vvv debug.|
|-P,--print-loaded-vars        | Print a dump of loaded vars, util for debug.|
|-h, --help                    | Get help for commands (show this help).|
|-V, --version                 | Show version number and quit|

### Examples
#### Print data in stdout formatted as text

```
[user@host get_embalse_status]$ ./get_embalse_status.sh --id=1234 -T --silent
────────────────────────────────────────────────────────────
Estado do encoro Barrie de la Maza:
────────────────────────────────────────────────────────────
Capacidade total:              31hm3    100%
Auga embalsada a 04/09/2023:   16hm3  51.61%
Variación coa semán anterior:  2hm3    6.45%
Mesma semán (2022):            7hm3   22.58%
Mesma semán (Med. 10 Anos):    11hm3  36.45%
────────────────────────────────────────────────────────────
Fontes:
Gráfica anual                 :  www.embalses.net/cache/pantano-1234.png?a=04-09-2023
embalses.net                  :  https://www.embalses.net/pantano-1234-barrie-de-la-maza.html
Boletín hidrolóxico semanal   :  https://www.miteco.gob.es/gl/agua/temas/evaluacion-de-los-recursos-hidricos/boletin-hidrologico.html
```

#### Send Telegram message in HTML format

```
[user@host get_embalse_status]$ ./get_embalse_status.sh --id=1234 -S
```
